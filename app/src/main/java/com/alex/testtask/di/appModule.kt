package com.alex.testtask.di

import com.alex.testtask.BuildConfig
import com.alex.testtask.data.remote.MealApi
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {

    single {
        provideRetrofit()
    }

    single {
        provideMealApi(get())
    }

}

fun provideRetrofit(): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BuildConfig.API_URL)
        .client(OkHttpClient())
        .build()
}

fun provideMealApi(retrofit: Retrofit): MealApi {
    return retrofit.create(MealApi::class.java)
}