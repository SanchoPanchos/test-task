package com.alex.testtask.di

import com.alex.testtask.repository.MealRepository
import com.alex.testtask.repository.RemoteMealRepository
import org.koin.dsl.module

val repositoryModule = module {

    single {
        RemoteMealRepository(get(), get()) as MealRepository
    }

}
