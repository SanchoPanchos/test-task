package com.alex.testtask.di

import com.alex.testtask.ui.mealsList.MealsListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        MealsListViewModel(get())
    }

}