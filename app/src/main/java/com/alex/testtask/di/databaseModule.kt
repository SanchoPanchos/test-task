package com.alex.testtask.di

import android.app.Application
import androidx.room.Room
import com.alex.testtask.data.persistence.AppDatabase
import com.alex.testtask.data.persistence.MealsDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {

    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "database").build()
    }

    fun provideDao(database: AppDatabase): MealsDao {
        return database.mealsDao()
    }

    single { provideDatabase(androidApplication()) }
    single { provideDao(get()) }

}
