package com.alex.testtask.repository

import androidx.lifecycle.LiveData
import com.alex.testtask.base.Result
import com.alex.testtask.data.remote.model.MealsResponse
import com.alex.testtask.ui.mealsList.MealUiModel

interface MealRepository {

    suspend fun loadMeals(query: String): Result<MealsResponse>

    suspend fun loadMealsHistory(): LiveData<List<MealUiModel>>

    suspend fun saveToRecent(mealUiModel: MealUiModel)

}