package com.alex.testtask.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.alex.testtask.base.Result
import com.alex.testtask.data.persistence.MealsDao
import com.alex.testtask.data.persistence.model.MealIngredientCrossRef
import com.alex.testtask.data.persistence.model.MealModel
import com.alex.testtask.data.remote.MealApi
import com.alex.testtask.data.remote.model.MealsResponse
import com.alex.testtask.ui.mealsList.MealUiModel
import com.alex.testtask.util.safeApiCall

class RemoteMealRepository(private val api: MealApi, private val mealsDao: MealsDao) :
    MealRepository {

    override suspend fun loadMeals(query: String): Result<MealsResponse> {
        return safeApiCall {
            api.getMeals(query)
        }
    }

    override suspend fun loadMealsHistory(): LiveData<List<MealUiModel>> {
        return Transformations.map(mealsDao.getAll()) {
            it.map { mealWithIngredient ->
                val meal = mealWithIngredient.meal
                MealUiModel(
                    meal.mealId,
                    meal.name,
                    meal.instruction,
                    meal.imageUrl,
                    meal.category,
                    meal.area,
                    mealWithIngredient.ingredients
                )
            }
        }
    }

    override suspend fun saveToRecent(mealUiModel: MealUiModel) {
        mealsDao.insertMeal(
            MealModel(
                mealUiModel.mealId,
                mealUiModel.name,
                mealUiModel.instruction,
                mealUiModel.imageUrl,
                mealUiModel.category,
                mealUiModel.area
            )
        )
        for (ingredient in mealUiModel.ingredients) {
            mealsDao.insertIngredient(ingredient)
            mealsDao.insertMealIngredient(
                MealIngredientCrossRef(
                    ingredient.ingredientName,
                    mealUiModel.mealId
                )
            )
        }
    }

}