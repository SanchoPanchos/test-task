package com.alex.testtask.data.persistence.model

import androidx.room.Entity

@Entity(primaryKeys = ["ingredientName", "mealId"])
data class MealIngredientCrossRef(
    val ingredientName: String,
    val mealId: Long
)
