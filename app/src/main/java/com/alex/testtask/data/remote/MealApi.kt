package com.alex.testtask.data.remote

import com.alex.testtask.data.remote.model.MealsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MealApi {

    @GET("search.php")
    suspend fun getMeals(@Query("s") query: String): MealsResponse

}