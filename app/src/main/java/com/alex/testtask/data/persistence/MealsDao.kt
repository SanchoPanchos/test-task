package com.alex.testtask.data.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import com.alex.testtask.data.persistence.model.IngredientModel
import com.alex.testtask.data.persistence.model.MealIngredientCrossRef
import com.alex.testtask.data.persistence.model.MealModel
import com.alex.testtask.data.persistence.model.MealWIthIngredients

@Dao
interface MealsDao {

    @Transaction
    @Query("SELECT * FROM MealModel")
    fun getAll(): LiveData<List<MealWIthIngredients>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMealIngredient(vararg mealIngredients: MealIngredientCrossRef)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMeal(vararg meals: MealModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIngredient(vararg ingredients: IngredientModel)

}