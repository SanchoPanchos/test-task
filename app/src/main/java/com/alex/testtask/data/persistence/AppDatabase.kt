package com.alex.testtask.data.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.alex.testtask.data.persistence.model.IngredientModel
import com.alex.testtask.data.persistence.model.MealIngredientCrossRef
import com.alex.testtask.data.persistence.model.MealModel

@Database(entities = [MealModel::class, MealIngredientCrossRef::class, IngredientModel::class], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun mealsDao(): MealsDao
}