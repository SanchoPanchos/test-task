package com.alex.testtask.data.persistence.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class IngredientModel(
    @PrimaryKey
    val ingredientName: String,
    val measure: String
)