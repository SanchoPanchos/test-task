package com.alex.testtask.data.persistence.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MealModel(
    @PrimaryKey
    val mealId: Long,
    val name: String,
    val instruction: String,
    val imageUrl: String,
    val category: String,
    val area: String
)