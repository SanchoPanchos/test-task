package com.alex.testtask.data.remote.model

import com.google.gson.annotations.SerializedName

data class Meal(
    @SerializedName("idMeal")
    val mealId: Long,
    @SerializedName("strMeal")
    val name: String,
    @SerializedName("strCategory")
    val category: String,
    @SerializedName("strArea")
    val area: String,
    @SerializedName("strInstructions")
    val instruction: String,
    @SerializedName("strMealThumb")
    val imageUrl: String,
    @SerializedName("strIngredient1")
    val ingredient1Name: String? = null,
    @SerializedName("strMeasure1")
    val ingredient1Measure: String? = null,
    @SerializedName("strIngredient2")
    val ingredient2Name: String? = null,
    @SerializedName("strMeasure2")
    val ingredient2Measure: String? = null,
    @SerializedName("strIngredient3")
    val ingredient3Name: String? = null,
    @SerializedName("strMeasure3")
    val ingredient3Measure: String? = null,
    @SerializedName("strIngredient4")
    val ingredient4Name: String? = null,
    @SerializedName("strMeasure4")
    val ingredient4Measure: String? = null,
    @SerializedName("strIngredient5")
    val ingredient5Name: String? = null,
    @SerializedName("strMeasure5")
    val ingredient5Measure: String? = null,
    @SerializedName("strIngredient6")
    val ingredient6Name: String? = null,
    @SerializedName("strMeasure6")
    val ingredient6Measure: String? = null,
    @SerializedName("strIngredient7")
    val ingredient7Name: String? = null,
    @SerializedName("strMeasure7")
    val ingredient7Measure: String? = null,
    @SerializedName("strIngredient8")
    val ingredient8Name: String? = null,
    @SerializedName("strMeasure8")
    val ingredient8Measure: String? = null,
    @SerializedName("strIngredient9")
    val ingredient9Name: String? = null,
    @SerializedName("strMeasure9")
    val ingredient9Measure: String? = null,
    @SerializedName("strIngredient10")
    val ingredient10Name: String? = null,
    @SerializedName("strMeasure10")
    val ingredient10Measure: String? = null,
    @SerializedName("strIngredient11")
    val ingredient11Name: String? = null,
    @SerializedName("strMeasure11")
    val ingredient11Measure: String? = null,
    @SerializedName("strIngredient12")
    val ingredient12Name: String? = null,
    @SerializedName("strMeasure12")
    val ingredient12Measure: String? = null,
    @SerializedName("strIngredient13")
    val ingredient13Name: String? = null,
    @SerializedName("strMeasure13")
    val ingredient13Measure: String? = null,
    @SerializedName("strIngredient14")
    val ingredient14Name: String? = null,
    @SerializedName("strMeasure14")
    val ingredient14Measure: String? = null,
    @SerializedName("strIngredient15")
    val ingredient15Name: String? = null,
    @SerializedName("strMeasure15")
    val ingredient15Measure: String? = null,
    @SerializedName("strIngredient16")
    val ingredient16Name: String? = null,
    @SerializedName("strMeasure16")
    val ingredient16Measure: String? = null,
    @SerializedName("strIngredient17")
    val ingredient17Name: String? = null,
    @SerializedName("strMeasure17")
    val ingredient17Measure: String? = null,
    @SerializedName("strIngredient18")
    val ingredient18Name: String? = null,
    @SerializedName("strMeasure18")
    val ingredient18Measure: String? = null,
    @SerializedName("strIngredient19")
    val ingredient19Name: String? = null,
    @SerializedName("strMeasure19")
    val ingredient19Measure: String? = null,
    @SerializedName("strIngredient20")
    val ingredient20Name: String? = null,
    @SerializedName("strMeasure20")
    val ingredient20Measure: String? = null
) {

    fun getMealIngredientPairs(): List<Pair<String, String>> {
        val pairs = mutableListOf<Pair<String?, String?>>()
        pairs.add(Pair(ingredient1Name, ingredient1Measure))
        pairs.add(Pair(ingredient2Name, ingredient2Measure))
        pairs.add(Pair(ingredient3Name, ingredient3Measure))
        pairs.add(Pair(ingredient4Name, ingredient4Measure))
        pairs.add(Pair(ingredient5Name, ingredient5Measure))
        pairs.add(Pair(ingredient6Name, ingredient6Measure))
        pairs.add(Pair(ingredient7Name, ingredient7Measure))
        pairs.add(Pair(ingredient8Name, ingredient8Measure))
        pairs.add(Pair(ingredient9Name, ingredient9Measure))
        pairs.add(Pair(ingredient10Name, ingredient10Measure))
        pairs.add(Pair(ingredient11Name, ingredient11Measure))
        pairs.add(Pair(ingredient12Name, ingredient12Measure))
        pairs.add(Pair(ingredient13Name, ingredient13Measure))
        pairs.add(Pair(ingredient14Name, ingredient14Measure))
        pairs.add(Pair(ingredient15Name, ingredient15Measure))
        pairs.add(Pair(ingredient16Name, ingredient16Measure))
        pairs.add(Pair(ingredient17Name, ingredient17Measure))
        pairs.add(Pair(ingredient18Name, ingredient18Measure))
        pairs.add(Pair(ingredient19Name, ingredient19Measure))
        pairs.add(Pair(ingredient20Name, ingredient20Measure))

        return pairs.filter {
            !it.first.isNullOrEmpty() && !it.second.isNullOrEmpty()
        }.map {
            Pair(it.first!!, it.second!!)
        }
    }

}