package com.alex.testtask.data.remote.model

import com.google.gson.annotations.SerializedName

data class MealsResponse(@SerializedName("meals") val meals: List<Meal> = listOf())