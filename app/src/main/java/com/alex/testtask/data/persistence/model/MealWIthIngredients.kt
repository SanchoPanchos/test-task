package com.alex.testtask.data.persistence.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MealWIthIngredients(
    @Embedded val meal: MealModel,
    @Relation(
        parentColumn = "mealId",
        entityColumn = "ingredientName",
        associateBy = Junction(MealIngredientCrossRef::class)
    )
    val ingredients: List<IngredientModel>
)