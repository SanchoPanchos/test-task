package com.alex.testtask

import android.app.Application
import com.alex.testtask.di.viewModelModule
import com.alex.testtask.di.appModule
import com.alex.testtask.di.databaseModule
import com.alex.testtask.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TestApplication)
            modules(appModule, databaseModule, repositoryModule, viewModelModule)
        }
    }

}