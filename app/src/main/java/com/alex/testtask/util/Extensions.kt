package com.alex.testtask.util

import android.os.Build
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import com.alex.testtask.R
import com.alex.testtask.base.BaseFragment
import com.alex.testtask.base.Result

private val defaultNavOptionsBuilder: NavOptions.Builder =
    NavOptions.Builder()
        .setEnterAnim(R.anim.slide_in_right)
        .setExitAnim(R.anim.slide_out_left)
        .setPopEnterAnim(R.anim.slide_in_left)
        .setPopExitAnim(R.anim.slide_out_right)

fun Fragment.showToast(message: String?) {
    if (message == null) {
        Toast.makeText(context, R.string.default_error, Toast.LENGTH_SHORT).show()
    } else {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}

fun BaseFragment.navigateTo(navId: Int) {
    navigateTo(navId, null)
}

fun BaseFragment.navigateTo(navDirections: NavDirections) {
    navigateTo(navDirections.actionId, navDirections.arguments)
}

fun BaseFragment.navigateTo(navId: Int, bundle: Bundle?) {
    this.navController.navigate(navId, bundle, defaultNavOptionsBuilder.build())
}

fun TextView.textAppearance(styleId: Int) {
    if (Build.VERSION.SDK_INT < 23) {
        setTextAppearance(context, styleId)
    } else {
        setTextAppearance(styleId)
    }
}

suspend fun <T> safeApiCall(apiCall: suspend () -> T): Result<T> {
    return try {
        Result.success(apiCall.invoke())
    } catch (throwable: Throwable) {
        Result.error(throwable.message)
    }

}
