package com.alex.testtask.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    protected val _dataLoadingLiveData = MutableLiveData<Boolean>()
    val dataLoadingLiveData: LiveData<Boolean> = _dataLoadingLiveData

    protected val _errorMessageLiveData = MutableLiveData<String?>()
    val errorMessageLiveData: LiveData<String?> = _errorMessageLiveData

}