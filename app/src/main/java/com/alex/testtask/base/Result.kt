package com.alex.testtask.base

class Result<T> {

    var status: Status
    var data: T? = null
    var error: String? = null

    val isSuccessful: Boolean
        get() = status == Status.SUCCESS

    constructor(error: String?) {
        this.status = Status.ERROR
        this.error = error
    }

    constructor(data: T) {
        this.data = data
        this.status = Status.SUCCESS
    }

    companion object {
        fun <T> success(data: T): Result<T> {
            return Result(data)
        }

        fun <T> error(error: String?): Result<T> {
            return Result(error)
        }
    }
}

enum class Status {
    SUCCESS,
    ERROR
}