package com.alex.testtask.ui.mealsList

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alex.testtask.R
import com.alex.testtask.base.BaseFragment
import com.alex.testtask.util.navigateTo
import com.alex.testtask.util.showToast
import kotlinx.android.synthetic.main.fragment_meal_list.*
import org.koin.android.ext.android.inject

class MealsListFragment : BaseFragment(R.layout.fragment_meal_list) {

    private lateinit var mealsAdapter: MealsListAdapter
    private lateinit var recentMealsAdapter: RecentMealsAdapter

    private val viewModel: MealsListViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclers()
        initListeners()
        initViewModelListeners()
    }

    private fun initViewModelListeners() {
        viewModel.recentMealsLiveData.observe(viewLifecycleOwner, Observer {
            onRecentMealsLoaded(it)
        })
        viewModel.mealsLiveData.observe(viewLifecycleOwner, Observer {
            onMealsLoaded(it)
        })
        viewModel.dataLoadingLiveData.observe(viewLifecycleOwner, Observer {
            showLoader(it)
        })
        viewModel.errorMessageLiveData.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })
    }

    private fun initListeners() {
        etSearch.addTextChangedListener {
            if (it.isNullOrEmpty()) {
                recentMealsRecycler.visibility = View.VISIBLE
                tvRecent.visibility = View.VISIBLE
                return@addTextChangedListener
            }

            recentMealsRecycler.visibility = View.GONE
            tvRecent.visibility = View.GONE
            viewModel.searchMeals(it.toString())
        }
    }

    private fun initRecyclers() {
        mealsAdapter = MealsListAdapter {
            onMealClicked(it)
        }
        mealsRecycler.layoutManager = LinearLayoutManager(context)
        mealsRecycler.adapter = mealsAdapter

        recentMealsAdapter = RecentMealsAdapter {
            onMealClicked(it)
        }
        recentMealsRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recentMealsRecycler.adapter = recentMealsAdapter
    }

    private fun onMealClicked(meal: MealUiModel) {
        viewModel.saveToRecent(meal)
        navigateTo(MealsListFragmentDirections.toMealDetails(meal))
    }

    private fun onMealsLoaded(mealList: List<MealUiModel>) {
        mealsAdapter.submitList(mealList)
    }

    private fun onRecentMealsLoaded(mealList: List<MealUiModel>) {
        tvRecent.visibility = View.VISIBLE
        recentMealsAdapter.submitList(mealList)
    }

    private fun showLoader(showLoader: Boolean) {
        if (showLoader) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

}