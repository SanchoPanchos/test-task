package com.alex.testtask.ui.mealsList

import com.alex.testtask.data.persistence.model.IngredientModel
import java.io.Serializable

data class MealUiModel(
    val mealId: Long,
    val name: String,
    val instruction: String,
    val imageUrl: String,
    val category: String,
    val area: String,
    val ingredients: List<IngredientModel>
) : Serializable