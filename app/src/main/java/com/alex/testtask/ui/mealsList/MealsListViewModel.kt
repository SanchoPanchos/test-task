package com.alex.testtask.ui.mealsList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.alex.testtask.base.BaseViewModel
import com.alex.testtask.data.persistence.model.IngredientModel
import com.alex.testtask.data.remote.model.Meal
import com.alex.testtask.repository.MealRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MealsListViewModel(private val mealsRepository: MealRepository) : BaseViewModel() {

    private val _mealsLiveData: MutableLiveData<List<MealUiModel>> = MutableLiveData()
    val mealsLiveData: LiveData<List<MealUiModel>> = _mealsLiveData

    val recentMealsLiveData: LiveData<List<MealUiModel>> = liveData {
        emitSource(mealsRepository.loadMealsHistory())
    }

    init {
        searchMeals("")
    }

    fun searchMeals(query: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _dataLoadingLiveData.postValue(true)
            val result = mealsRepository.loadMeals(query)
            if (result.isSuccessful) {
                _mealsLiveData.postValue(map(result.data?.meals))
            } else {
                _errorMessageLiveData.postValue(result.error)
            }
            _dataLoadingLiveData.postValue(false)
        }
    }

    private fun map(meals: List<Meal>?): List<MealUiModel> {
        return meals?.map { meal ->
            MealUiModel(
                meal.mealId,
                meal.name,
                meal.instruction,
                meal.imageUrl,
                meal.category,
                meal.area,
                meal.getMealIngredientPairs().map { ingredientPair ->
                    IngredientModel(
                        ingredientPair.first,
                        ingredientPair.second
                    )
                })
        } ?: listOf()
    }

    fun saveToRecent(meal: MealUiModel) {
        viewModelScope.launch(Dispatchers.IO) {
            mealsRepository.saveToRecent(meal)
        }
    }

}