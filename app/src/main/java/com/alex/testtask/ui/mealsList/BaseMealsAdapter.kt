package com.alex.testtask.ui.mealsList

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseMealsAdapter<T : RecyclerView.ViewHolder>(protected val mealClickListener: OnMealClickListener) : RecyclerView.Adapter<T>() {

    protected val meals: MutableList<MealUiModel> = mutableListOf()

    override fun getItemCount() = meals.size

    fun submitList(newMealsList: List<MealUiModel>) {
        val result = DiffUtil.calculateDiff(MealDiffUtilCallback(meals, newMealsList))
        meals.clear()
        meals.addAll(newMealsList)
        result.dispatchUpdatesTo(this)
    }

}

typealias OnMealClickListener = (meal: MealUiModel) -> Unit