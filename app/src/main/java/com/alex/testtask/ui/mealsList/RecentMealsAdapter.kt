package com.alex.testtask.ui.mealsList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alex.testtask.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_recent_meal.view.*

class RecentMealsAdapter(mealClickListener: OnMealClickListener) :
    BaseMealsAdapter<RecentMealsAdapter.MealViewHolder>(mealClickListener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_recent_meal, parent, false)
        return MealViewHolder(view)
    }

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) =
        holder.bind(meals[position])

    inner class MealViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            itemView.setOnClickListener {
                mealClickListener.invoke(meals[adapterPosition])
            }
        }

        fun bind(meal: MealUiModel) {
            with(itemView) {
                Glide.with(this)
                    .load(meal.imageUrl)
                    .into(ivMealImage)
                tvMealName.text = meal.name
            }
        }
    }

}