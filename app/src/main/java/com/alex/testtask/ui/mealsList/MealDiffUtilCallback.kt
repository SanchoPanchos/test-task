package com.alex.testtask.ui.mealsList

import androidx.recyclerview.widget.DiffUtil

class MealDiffUtilCallback(
    private val oldList: List<MealUiModel>,
    private val newList: List<MealUiModel>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size
    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].mealId == newList[newItemPosition].mealId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areItemsTheSame(oldItemPosition, newItemPosition)
    }
}