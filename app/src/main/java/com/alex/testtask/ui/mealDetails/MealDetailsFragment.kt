package com.alex.testtask.ui.mealDetails

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.navArgs
import com.alex.testtask.R
import com.alex.testtask.base.BaseFragment
import com.alex.testtask.data.persistence.model.IngredientModel
import com.alex.testtask.util.DisplayUtil
import com.alex.testtask.util.textAppearance
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_meal_details.*

class MealDetailsFragment : BaseFragment(R.layout.fragment_meal_details) {

    private val args: MealDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMealData()
    }

    private fun setMealData() {
        with(args.meal) {
            Glide.with(this@MealDetailsFragment)
                .load(imageUrl)
                .into(ivMealImage)

            setIngredients(ingredients)
            tvMealDescription.text = instruction
            tvMealArea.text = area
            tvMealCategory.text = category
        }
    }

    private fun setIngredients(ingredients: List<IngredientModel>) {
        val views = createIngredientViews(ingredients)
        if (views.isEmpty()) {
            return
        }

        constraint.post {
            val set = ConstraintSet()
            set.clone(constraint)

            positionFirstIngredient(set, views[0])
            for (i in 1 until views.size) {
                positionIngredient(set, views[i], views[i - 1])
            }
            positionMealDescription(set, views[views.size - 1])

            set.applyTo(constraint)
        }
    }

    private fun positionMealDescription(set: ConstraintSet, textView: TextView) {
        set.connect(tvMealDescription.id, ConstraintSet.TOP, textView.id, ConstraintSet.BOTTOM)
    }

    private fun positionIngredient(
        set: ConstraintSet,
        textView: TextView,
        previousTextView: TextView
    ) {
        set.connect(textView.id, ConstraintSet.START, constraint.id, ConstraintSet.START)
        set.connect(textView.id, ConstraintSet.END, constraint.id, ConstraintSet.END)
        set.connect(textView.id, ConstraintSet.TOP, previousTextView.id, ConstraintSet.BOTTOM)
    }

    private fun positionFirstIngredient(set: ConstraintSet, textView: TextView) {
        val margin = DisplayUtil.dpToPx(requireContext(), 16)
        set.connect(textView.id, ConstraintSet.START, constraint.id, ConstraintSet.START)
        set.connect(textView.id, ConstraintSet.END, constraint.id, ConstraintSet.END)
        set.connect(textView.id, ConstraintSet.TOP, ivArea.id, ConstraintSet.BOTTOM, margin)
    }

    private fun createIngredientViews(ingredients: List<IngredientModel>): List<TextView> {
        val views = mutableListOf<TextView>()
        val margin = DisplayUtil.dpToPx(requireContext(), 16)
        val colorFirst = ContextCompat.getColor(requireContext(), R.color.colorPrimary)
        val colorSecond = ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
        for ((index, ingredient) in ingredients.withIndex()) {
            views.add(TextView(requireContext()).apply {
                id = View.generateViewId()
                text = "${ingredient.ingredientName} -- ${ingredient.measure}"
                layoutParams =
                    ConstraintLayout.LayoutParams(0, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                constraint.addView(this, 0)
                setPadding(margin, margin / 2, margin, margin / 2)
                textAppearance(R.style.TextNormal)
                setBackgroundColor(
                    if (index.rem(2) == 0) {
                        colorFirst
                    } else {
                        colorSecond
                    }
                )
            })
        }
        return views
    }
}