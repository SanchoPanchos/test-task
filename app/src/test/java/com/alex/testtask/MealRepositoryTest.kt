package com.alex.testtask

import com.alex.testtask.base.Result
import com.alex.testtask.data.persistence.MealsDao
import com.alex.testtask.data.remote.MealApi
import com.alex.testtask.data.remote.model.MealsResponse
import com.alex.testtask.repository.MealRepository
import com.alex.testtask.repository.RemoteMealRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.only
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class MealRepositoryTest {

    private lateinit var repository: MealRepository
    private lateinit var mealApi: MealApi
    private lateinit var mealsDao: MealsDao

    private val searchQuery = "beef"

    private val mealsResponse =
        MealsResponse()
    private val successResult = Result.success(mealsResponse)

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        mealApi = mock()
        mealsDao = mock()
        repository = RemoteMealRepository(mealApi, mealsDao)
        runBlocking {
            whenever(mealApi.getMeals(searchQuery)).thenReturn(mealsResponse)
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun testMealsLoadedSuccess() {
        runBlocking {
            val result = repository.loadMeals(searchQuery)
            verify(mealApi, only()).getMeals(searchQuery)
            assert(result.isSuccessful)
            assertEquals(result.data, successResult.data)
        }
    }
}